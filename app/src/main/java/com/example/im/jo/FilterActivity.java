package com.example.im.jo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;

import com.thomashaertel.widget.MultiSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FilterActivity extends AppCompatActivity {

    public static final String TAG = "FilterActivity";

    public static final String OPENED_ONLY = "opened_only";

    public static final String HAS_DELIVERY = "has_delivery";

    public static final String ORDER_PARAM = "order";

    public static final String PLACES_TAGS = "places_tags";

    private HashMap<String, String> tags;

    private ArrayList<String> selectedTags;

    private Switch openedSwitch;

    private Switch hasDeliverySwitch;

    private MultiSpinner tagsSelector;

    private Spinner orderSpinner;

    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        PlacesApplication app = (PlacesApplication)getApplication();
        tags = app.getTagsList();
        openedSwitch = (Switch)findViewById(R.id.openedSwitch);
        hasDeliverySwitch = (Switch)findViewById(R.id.hasDeliverySwitch);

        orderSpinner = (Spinner)findViewById(R.id.orderSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.order_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orderSpinner.setAdapter(adapter);

        Button okBtn = (Button)findViewById(R.id.okBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(v);
            }
        });

        Button cancelBtn = (Button)findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        Log.d(TAG, "Checking extras: " + extras.toString());
        if (extras != null) {
            openedSwitch.setChecked(extras.getBoolean(OPENED_ONLY));
            hasDeliverySwitch.setChecked(extras.getBoolean(HAS_DELIVERY));
            String selectedTagsStr = extras.getString(PLACES_TAGS);
            orderSpinner.setSelection(extras.getInt(ORDER_PARAM, 0));

            selectedTags = new ArrayList<String>();
            if (selectedTagsStr != null && selectedTagsStr.length() > 0) {
                selectedTags.addAll(Arrays.asList(selectedTagsStr.split(",")));
            }
        }
        tagsSelectorInit();
    }

    private void tagsSelectorInit() {
        // create spinner list elements
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

        boolean[] selectedItems = new boolean[tags.size()];
        int index = 0;
        boolean isAnySelected = false;
        for (Map.Entry<String, String> entry : tags.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            adapter.add(value);
            if (selectedTags.contains(key)) {
                selectedItems[index] = true;
                isAnySelected = true;
            }
            index++;
        }

        // get spinner and set adapter
        tagsSelector = (MultiSpinner) findViewById(R.id.tagsSelector);
        tagsSelector.setAdapter(adapter, false, onSelectedListener);
        tagsSelector.setSelected(selectedItems);
        tagsSelector.setDefaultText(getResources().getString(R.string.no_tags_selected));
        if (isAnySelected == false) {
            tagsSelector.setText(R.string.no_tags_selected);
        }
    }

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            selectedTags.clear();
            int index = 0;
            for (String key : tags.keySet()) {
                if (selected[index] == true) {
                    selectedTags.add(key);
                }
                index++;
            }
        }
    };

    private void sendResult(View view) {
        Log.d(TAG, "OK button clicked");
        Intent resultIntent = new Intent();

        resultIntent.putExtra(OPENED_ONLY, openedSwitch.isChecked());
        resultIntent.putExtra(HAS_DELIVERY, hasDeliverySwitch.isChecked());
        resultIntent.putExtra(PLACES_TAGS, TextUtils.join(",", selectedTags));
        resultIntent.putExtra(ORDER_PARAM, orderSpinner.getSelectedItemPosition());

        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
