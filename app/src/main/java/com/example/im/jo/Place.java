package com.example.im.jo;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

/**
 * Created by im on 7.10.15.
 */
public class Place {
    private int id;
    private LatLng position;

    public String name;
    public String description;
    public String address;
    public String tagging;
    public String workingTime;
    public Integer isOpened;
    public Boolean hasDelivery;
    public Double distance;
    public String bookingNumber;
    public int pricing;
    public int recommendations;

    public Marker marker;

    public Place() {
        super();
    }

    public Place(int id, LatLng position) {
        super();
        this.id = id;
        this.position = position;
    }

    public int getId() {
        return this.id;
    }

    public LatLng getPosition() {
        return this.position;
    }

    public String getDistanceText() {
        String distanceText = String.format("%.2f", (this.distance / 1000)) + " km";
        return distanceText;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
