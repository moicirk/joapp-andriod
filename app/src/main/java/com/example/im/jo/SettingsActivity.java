package com.example.im.jo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Locale;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();

        /*PlacesApplication app = (PlacesApplication)getApplication();
        app.setLocaleListener("mapsActivity", new PlacesApplication.LocaleChangeListener() {
            @Override
            public void onChange(Locale locale) {
                recreate();
            }
        });*/
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            Preference languagePref = findPreference("language");
            languagePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String language = newValue.toString();
                    Log.d("SETTINGS", "Got new language setting: " + language);
                    PlacesApplication app = (PlacesApplication) getActivity().getApplication();
                    app.setLocale(language);
                    getActivity().recreate();
                    return true;
                }
            });
        }
    }
}
