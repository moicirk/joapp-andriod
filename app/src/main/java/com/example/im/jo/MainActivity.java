package com.example.im.jo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private PlacesApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (PlacesApplication) getApplication();

        if (hasConnection()) {
            setContentView(R.layout.activity_main);
            loadApp();
        } else {
            setContentView(R.layout.activity_no_connection);
            showNoConnectionWarning();
            Button reloadBtn = (Button)findViewById(R.id.reloadBtn);
            reloadBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hasConnection()) {
                        loadApp();
                    }
                    else {
                        showNoConnectionWarning();
                    }
                }
            });
        }
    }

    private void showNoConnectionWarning()
    {
        String message = getResources().getString(R.string.no_internet_message);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void loadApp() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        app.setLocale(prefs.getString("language", "en"));

        TagsRequest request = new TagsRequest(this);
        request.execute(PlacesApplication.TAG_REQUEST_URL);
    }

    protected boolean hasConnection() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = manager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private class TagsRequest extends RestRequestTask {

        private Context context;

        public TagsRequest(Context context) {
            this.context = context;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            Log.d(TAG, "Result: " + result.toString());
            HashMap<String, String> tags = app.getTagsList();

            try {
                for(int i=0; i < result.length(); i++){
                    JSONObject tag = result.getJSONObject(i);
                    tags.put(tag.optString("name"), tag.optString("label"));
                }
            } catch (JSONException e) {
                Log.e(TAG, "JSONException: " + e.getMessage());
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

            Intent intent = new Intent(context, LocationActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
