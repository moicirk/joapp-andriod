package com.example.im.jo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MapsActivity extends ActionBarActivity {

    public static final String TAG = "MapsActivity";
    private static final String REQUEST_URL = "http://dev.imcount.com/api/places";

    private PlacesApplication app;
    private GoogleMap googleMap;
    private LatLng currentLocation;
    private HorizontalPager placePager;
    private HashMap<Marker, Place> markerPlaceMap;
    private Integer currentPlaceIndex = 0;

    private Boolean openedOnly = false;
    private Boolean hasDelivery = false;
    private int order = 0;
    private String selectedTags;
    private SlidingUpPanelLayout mLayout;
    private boolean slidingPanelOpened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        app = (PlacesApplication)getApplication();
        app.setLocaleListener("mapsActivity", new PlacesApplication.LocaleChangeListener() {
            @Override
            public void onChange(Locale locale) {
                Log.d(TAG, "Locale updated to " + locale.getLanguage() + ", forcing to reload");
                recreate();
            }
        });

        Bundle extras = getIntent().getExtras();
        Log.d(TAG, "Checking extras: " + extras.toString());
        if (extras != null) {
            double latitude = extras.getDouble("current_latitude");
            double longitude = extras.getDouble("current_longitude");
            Log.d(TAG, "Coordinates received: latitude = " + latitude + "; longitude = " + longitude);
            currentLocation = new LatLng(latitude, longitude);
        }

        setUpActionBar();
        setUpActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case (1) : {
                if (resultCode == Activity.RESULT_OK) {
                    openedOnly = data.getBooleanExtra(FilterActivity.OPENED_ONLY, false);
                    hasDelivery = data.getBooleanExtra(FilterActivity.HAS_DELIVERY, false);
                    selectedTags = data.getStringExtra(FilterActivity.PLACES_TAGS);
                    order = data.getIntExtra(FilterActivity.ORDER_PARAM, 0);
                    updatePlaces();
                }
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpActivity();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Stopping activity. Unsetting locale listener");
        app.unsetLocaleListener("mapsActivity");
    }

    @Override
    public void onBackPressed() {
        if (mLayout == null) {
            finish();
        } else {
            if (slidingPanelOpened) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            } else {
                finish();
            }
        }
    }

    private ArrayList<Place> getPlacesList() {
        return app.getPlacesList();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpActivity() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (googleMap != null) {
                googleMap.setMyLocationEnabled(false);
                googleMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                placePager = (HorizontalPager) findViewById(R.id.horizontal_pager);
                markerPlaceMap = new HashMap<Marker, Place>();

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    public boolean onMarkerClick(Marker arg0) {
                        return true;
                    }
                });

                placePager.setOnScreenSwitchListener(new HorizontalPager.OnScreenSwitchListener() {
                    @Override
                    public void onScreenSwitched(final int screen) {
                        highlightMarker(screen);
                    }
                });

                mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
                mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {

                    @Override
                    public void onPanelExpanded(View panel) {
                        placePager.setLocked(true);
                        slidingPanelOpened = true;
                    }

                    @Override
                    public void onPanelCollapsed(View panel) {
                        placePager.setLocked(false);
                        slidingPanelOpened = false;
                    }

                    @Override
                    public void onPanelSlide(View panel, float slideOffset) {
                    }

                    @Override
                    public void onPanelAnchored(View panel) {
                    }

                    @Override
                    public void onPanelHidden(View panel) {
                    }
                });

                updatePlaces();
            }
        }
    }

    private void setHomeMarker() {
        Log.d(TAG, "Setting home marker");

        googleMap.addMarker(new MarkerOptions()
                .position(currentLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_marker)));
    }

    /**
     * Moves camera to marker binded with selected
     * place in pager
     * @param placeIndex
     */
    private void highlightMarker(int placeIndex) {
        currentPlaceIndex = placeIndex;
        Place place = getPlacesList().get(placeIndex);
        if (place != null) {
            Log.d(TAG, "Place switched: " + place.name);
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(place.marker.getPosition()));
        }
    }

    private void updatePlaces() {
        Log.d(TAG, "Update places method called");

        PlacesRequest request = new PlacesRequest();
        String url = REQUEST_URL;
        url += "?coords=" + currentLocation.latitude + "," + currentLocation.longitude;
        url += "&order=" + order;

        if (openedOnly) {
            url += "&opened=1";
        }

        if (hasDelivery) {
            url += "&delivery=1";
        }

        if (selectedTags != null && selectedTags.length() > 0) {
            url += "&tags=" + selectedTags;
        }
        request.execute(url);
    }

    private void addPlace(Place place) {
        LatLng position = place.getPosition();
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.marker_offline);
        if (place.isOpened == 1) {
            icon = BitmapDescriptorFactory.fromResource(R.drawable.marker);
        }


        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(position)
                .title(place.name)
                .snippet(place.address)
                .icon(icon)
                .flat(true));

        place.marker = marker;
        getPlacesList().add(place);
        markerPlaceMap.put(marker, place);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View placeView = inflater.inflate(R.layout.place_item, null);

        ((TextView)placeView.findViewById(R.id.placeName)).setText(place.name);
        ((TextView)placeView.findViewById(R.id.placeDescription)).setText(place.description);
        ((TextView)placeView.findViewById(R.id.placeAddress)).setText(place.address);
        ((TextView)placeView.findViewById(R.id.placeDistance)).setText(place.getDistanceText());
        ((TextView)placeView.findViewById(R.id.placeRecommendationsAmount)).setText(Integer.toString(place.recommendations));

        if (place.isOpened == 1) {
            ImageView openedIcon = (ImageView)placeView.findViewById(R.id.placeOpenedIcon);
            openedIcon.setBackgroundResource(R.drawable.km_circle);
            openedIcon.setImageResource(R.drawable.ic_lock_open);
            openedIcon.setPadding(12, 12, 12, 12);
        }

        if (!place.workingTime.isEmpty()) {
            ((TextView)placeView.findViewById(R.id.placeWorkingTime)).setText(place.workingTime);
        }

        /* Prices tokens and string */
        ArrayList<Integer> priceTokens = new ArrayList<Integer>();
        priceTokens.add(R.drawable.price_on_token);
        if (place.pricing == 0) {
            priceTokens.add(R.drawable.price_off_token);
            priceTokens.add(R.drawable.price_off_token);
        }
        else if (place.pricing == 1) {
            priceTokens.add(R.drawable.price_on_token);
            priceTokens.add(R.drawable.price_off_token);
        }
        else if (place.pricing == 2) {
            priceTokens.add(R.drawable.price_on_token);
            priceTokens.add(R.drawable.price_on_token);
        }

        LinearLayout tokenPlaceholder = (LinearLayout)placeView.findViewById(R.id.tokenPlaceholder);
        for (int i = 0; i < priceTokens.size(); i++) {
            ImageView priceToken = new ImageView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 0, (int)getResources().getDimension(R.dimen.tokens_margin), 0);
            priceToken.setLayoutParams(params);
            priceToken.setImageResource(priceTokens.get(i));
            tokenPlaceholder.addView(priceToken);
        }

        String[] priceCategories = getResources().getStringArray(R.array.pricing_categories);
        ((TextView)placeView.findViewById(R.id.placePriceStr)).setText(priceCategories[place.pricing]);

        /* Buttons */
        final Place finalPlace = place;
        ((Button)placeView.findViewById(R.id.callActionBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + finalPlace.bookingNumber));
                startActivity(intent);
            }
        });

        placePager.addView(placeView);
    }

    private class PlacesRequest extends RestRequestTask {

        @Override
        protected void onPreExecute() {
            Log.d(TAG, "Starting external API request");
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            Log.d(TAG, "External API response received: " + result.toString());
            clearAll();

            try {
                for(int i=0; i < result.length(); i++){
                    Place place = parsePlaceObject(result.getJSONObject(i));
                    addPlace(place);
                }

                setHomeMarker();
                highlightMarker(0);
            } catch (JSONException e) {
                Log.e(TAG, "JSONException: " + e.getMessage());
            } catch (Exception e) {
                Log.e(TAG, "Exception:" + e.getMessage());
            }
        }

        /**
         * @param placeData
         * @return
         */
        private Place parsePlaceObject(JSONObject placeData) throws JSONException {
            LatLng position = new LatLng(
                    Double.parseDouble(placeData.optString("latitude").toString()),
                    Double.parseDouble(placeData.optString("longitude").toString())
            );

            Place place = new Place(Integer.parseInt(placeData.optString("id").toString()), position);

            place.name = placeData.optString("name");
            place.description = placeData.optString("description");
            place.address = placeData.optString("address");
            place.workingTime = placeData.optString("workingTimeLabel");
            place.isOpened = placeData.optInt("isOpened");
            place.hasDelivery = placeData.optBoolean("has_delivery");
            place.distance = placeData.optDouble("distance");
            place.bookingNumber = placeData.optString("booking_number");
            place.pricing = placeData.optInt("pricing");
            place.recommendations = placeData.optInt("recommendationsAmount");
            place.tagging = placeData.optString("tagging");

            return place;
        }

        private void clearAll() {
            Log.d(TAG, "Clear all method called");
            Log.d(TAG, "Clearing map");
            googleMap.clear();

            Log.d(TAG, "Clearing pager");
            placePager.setCurrentScreen(0, false);
            placePager.removeAllViews();

            Log.d(TAG, "Clearing places list");
            getPlacesList().clear();
        }
    }
}
