package com.example.im.jo;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by im on 13.10.15.
 */
public class PlacesApplication extends Application {

    public static final String TAG = "Application";

    public static final String TAG_REQUEST_URL = "http://dev.imcount.com/api/tags";

    private ArrayList<Place> places;
    private HashMap<String, String> tags;
    private HashMap<String, LocaleChangeListener> localeListeners;

    @Override
    public void onCreate() {
        super.onCreate();
        places = new ArrayList<Place>();
        tags = new HashMap<String, String>();
        localeListeners = new HashMap<String, LocaleChangeListener>();
    }

    public void setLocaleListener(String key, LocaleChangeListener listener) {
        if (localeListeners.containsKey(key) == false) {
            localeListeners.put(key, listener);
        }
    }

    public void unsetLocaleListener(String key) {
        if (localeListeners.containsKey(key) == false) {
            localeListeners.remove(key);
        }
    }

    public void setLocale(String language) {
        String country = "EE";
        Locale locale = new Locale(language, country);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        for (HashMap.Entry<String, LocaleChangeListener> entry : localeListeners.entrySet()) {
            String key = entry.getKey();
            LocaleChangeListener listener = entry.getValue();
            Log.d(TAG, "Calling locale change listener with key " + key);
            listener.onChange(locale);
        }
    }

    public ArrayList<Place> getPlacesList() {
        return this.places;
    }

    public HashMap<String, String> getTagsList() {
        return this.tags;
    }

    public interface LocaleChangeListener {
        void onChange(Locale locale);
    }
}
