package com.example.im.jo;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.joapp.views.AddressAutocomplete;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocationActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    public static final String TAG = "LocationActivity";

    private GoogleMap mMap;

    private GoogleApiClient mGoogleApiClient;
    private LatLng currentLocation;
    private LatLng defaultLocation;
    private Button confirmAddressBtn;
    private AddressAutocomplete locationAddress;
    private ArrayAdapter<String> autocompleteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        PlacesApplication app = (PlacesApplication)getApplication();
        defaultLocation = new LatLng(59.435707, 24.752557);
        setUpActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Reconnecting google api client on resume");
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Disconnecting google api client on pause");
            mGoogleApiClient.disconnect();
        }
    }

    private void setUpActivity() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.locationMap))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                Log.d(TAG, "Google map object loaded");
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setZoomControlsEnabled(true);

                autocompleteAdapter = new ArrayAdapter<String>(this, R.layout.address_autocomplete_item, new ArrayList<String>());
                autocompleteAdapter.setNotifyOnChange(false);

                locationAddress = (AddressAutocomplete)findViewById(R.id.locationAddress);
                locationAddress.setAdapter(autocompleteAdapter);
                locationAddress.setOnAddressSelectedListener(new AddressAutocomplete.OnAddressSelectedListener() {
                    @Override
                    public void onAddressSelected(Address address) {
                        Log.d(TAG, "onAddressSelected called");
                        LatLng coordinates = new LatLng(address.getLatitude(), address.getLongitude());
                        setCurrentLocation(coordinates);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));
                    }
                });

                confirmAddressBtn = (Button) findViewById(R.id.confirmButton);
                confirmAddressBtn.setOnClickListener(this);

                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();

                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                    private CameraPosition previousPosition;

                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        Log.d(TAG, "Camera position changed: " + cameraPosition.toString());

                        if (previousPosition == null || !cameraPosition.equals(previousPosition)) {
                            previousPosition = cameraPosition;
                            locationAddress.setAutocompleteEnabled(false);
                            setCurrentLocation(cameraPosition.target);
                            showCurrentAddress();
                        }
                    }
                });
            }
            else {
                Log.e(TAG, "Unable to load Google Map object");
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        if (currentLocation == null) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                Log.w(TAG, "Unable to define location");
                showNoLocationWarning();
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLocation, 15));
                setCurrentLocation(defaultLocation);
            }
            else {
                LatLng coordinates = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));
                setCurrentLocation(coordinates);
            }
        } else {
            Log.d(TAG, "Current location already defined");
            if(mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    private void showNoLocationWarning() {
        String message = getResources().getString(R.string.no_location_warning);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void setCurrentLocation(LatLng coordinates) {
        Log.d(TAG, "New location received: " + coordinates.toString());
        currentLocation = coordinates;
        confirmAddressBtn.setVisibility(View.VISIBLE);
    }

    private void showCurrentAddress() {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(currentLocation.latitude, currentLocation.longitude, 1);

            if(addresses.size() > 0) {
                Address address = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
                locationAddress.setText(TextUtils.join(",", addressFragments));
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "Location service suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Failed to connect to location service");
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "Confirm address button clicked");
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("current_latitude", currentLocation.latitude);
        intent.putExtra("current_longitude", currentLocation.longitude);

        Log.d(TAG, "Sending coordinates: latitude = " + currentLocation.latitude + "; longitude = " + currentLocation.longitude);
        startActivity(intent);
    }
}
