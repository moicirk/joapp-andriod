package com.joapp.views;

import android.content.Context;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 11/13/2015.
 */
public class AddressAutocomplete extends AutoCompleteTextView
    implements AdapterView.OnItemClickListener {

    private static final String LOG_TAG = "AddressAutocompleteView";
    private static final int AUTOCOMPLETE_THRESHOLD = 3;
    private static final int AUTOCOMPLETE_MAX_RESULT = 5;

    private Geocoder geocoder;
    private List<Address> addressesList;
    private boolean autocompleteEnabled = true;
    private LatLngBounds bounds;
    private boolean firstFocus = false;

    private OnAddressSelectedListener addressSelectedListener;

    public AddressAutocomplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(LOG_TAG, "Class initiated");
        init();
    }

    public void setOnAddressSelectedListener(OnAddressSelectedListener listener) {
        this.addressSelectedListener = listener;
    }

    public void setAutocompleteEnabled(boolean enabled) {
        if (enabled) {
            Log.d(LOG_TAG, "Autocomplete unlocked");
        } else {
            Log.d(LOG_TAG, "Autcomplete locked");
        }
        this.autocompleteEnabled = enabled;
    }

    public boolean getAutocompleteEnabled() {
        return this.autocompleteEnabled;
    }

    private void init() {
        setThreshold(AUTOCOMPLETE_THRESHOLD);
        geocoder = new Geocoder(getContext());
        bounds = new LatLngBounds(
                new LatLng(57.292215, 20.980418),
                new LatLng(59.912179, 28.758738)
        );

        setOnItemClickListener(this);
        addTextChangedListener(new AddressTextWatcher());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(LOG_TAG, "Item selected: " + Integer.toString(position) + "; amount: " + Integer.toString(addressesList.size()));
        if (addressesList.size() > position) {
            Address selectedAddress = addressesList.get(position);
            Log.d(LOG_TAG, "Address selected: " + selectedAddress.toString());
            if (addressSelectedListener != null) {
                Log.d(LOG_TAG, "Listener trigger with address: " + selectedAddress.toString());
                addressSelectedListener.onAddressSelected(selectedAddress);
            }
        }
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused && firstFocus) {
            setText("");
            firstFocus = false;
        }
    }

    private class AddressTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            final String search = s.toString();
            Log.d(LOG_TAG, "Autcomplete field updated with: " + search);
            ArrayAdapter<String> adapter = (ArrayAdapter<String>)getAdapter();
            adapter.clear();

            if (getAutocompleteEnabled() && !search.isEmpty() && search.length() >= getThreshold()) {
                try {
                    addressesList = geocoder.getFromLocationName(
                            search,
                            AUTOCOMPLETE_MAX_RESULT,
                            bounds.southwest.latitude,
                            bounds.southwest.longitude,
                            bounds.northeast.latitude,
                            bounds.northeast.longitude
                    );

                    for (Address a : addressesList) {
                        Log.v(LOG_TAG, "Address suggestion received: " + a.toString());
                        ArrayList<String> addressFragments = new ArrayList<String>();

                        for (int i = 0; i < a.getMaxAddressLineIndex(); i++) {
                            addressFragments.add(a.getAddressLine(i));
                        }
                        String temp = TextUtils.join(",", addressFragments);
                        adapter.add(temp);
                    }
                }
                catch (IOException e) {
                    Log.d(LOG_TAG, "Failed to get autocomplete suggestions", e);
                }
            }

            if (!getAutocompleteEnabled()) {
                setAutocompleteEnabled(true);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    public interface OnAddressSelectedListener {
        public void onAddressSelected(Address address);
    }
}
