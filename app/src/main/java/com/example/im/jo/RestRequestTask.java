package com.example.im.jo;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.HttpStatus;

/**
 * Created by im on 14.10.15.
 */
public class RestRequestTask extends AsyncTask<String, String, JSONArray> {

    protected static final String TAG = "RestRequestTask";

    @Override
    protected JSONArray doInBackground(String... params) {
        Log.d(TAG, "Requesting URL: " + params[0]);
        URL url;
        HttpURLConnection urlConnection = null;
        JSONArray response = new JSONArray();

        try {
            url = new URL(params[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            int responseCode = urlConnection.getResponseCode();

            if(responseCode == HttpStatus.SC_OK){
                String responseString = readStream(urlConnection.getInputStream());
                Log.v(TAG, responseString);
                response = new JSONArray(responseString);
            }else{
                Log.v(TAG, "Response code: "+ responseCode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Request exception: " + e.getMessage());
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
            Log.d(TAG, "URLconnection closed");
        }

        return response;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }
}
